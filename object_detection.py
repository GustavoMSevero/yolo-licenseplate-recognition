import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from google.colab.patches import cv2_imshow
import cv2

df = pd.read_csv('labels.csv')
df.head()


#             filepath	xmin	xmax	ymin	ymax
# 0	./images/N137.xml	395	595	453	503
# 1	./images/N208.xml	199	273	200	223
# 2	./images/N131.xml	197	350	105	145
# 3	./images/N102.xml	161	316	211	247
# 4	./images/N34.xml	558	903	479	600

import xml.etree.ElementTree as xet

filename = df['filepath'][0]
filename

# ./images/N137.xml

def getFilename(filename):
    filename_image = xet.parse(filename).getroot().find('filename').text
    filepath_image = os.path.join('./images', filename_image)
    return filepath_image

getFilename(filename)

# ./images/N137.jpeg

#Verify image and output

file_path = image_path[0]
file_path

# ./images/N137.jpeg

img = cv2.imread(file_path)

cv2.rectangle(img,(446,706),(660,792),(0,255,0),3)
cv2_imshow(img)
cv2.waitKey(0)
cv2.destroyAllWindows()

import tensorflow as tf
tf.__version__

# !sudo pip3 install keras

from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import load_img, img_to_array

labels = df.iloc[:,1:].values

data = []
output = []
for ind in range(len(image_path)):
    image = image_path[ind]
    img_arr = cv2.imread(image)
    h,w,d = img_arr.shape
    # preprocessing
    load_image = load_img(image, target_size=(224,224))
    load_image_arr = img_to_array(load_image)
    norm_load_image_arr = load_image_arr/255.0 # normalization
    # normalization to labels
    xmin,xmax,ymin,ymax = labels[ind]
    nxmin,nxmax = xmin/w,xmax/w
    nymin,nymax = ymin/w,ymax/w
    label_norm = (nxmin,nxmax,nymin,nymax) # normalization output
    # --------- append
    data.append(norm_load_image_arr)
    output.append(label_norm)

x = np.array(data,dtype=np.float32)
y = np.array(output,dtype=np.float32)

x.shape, y.shape

# ((228, 224, 224, 3), (228, 4))

x_train, x_test, y_train, y_test = train_test_split(x,y,train_size=0.8, random_state=0)
x_train.shape,x_test.shape,y_train.shape,y_test.shape

# ((182, 224, 224, 3), (46, 224, 224, 3), (182, 4), (46, 4))

# Deep Learning Model

from tensorflow.keras.applications import MobileNetV2, InceptionV3, InceptionResNetV2
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input
from tensorflow.keras.models import Model
import tensorflow as tf

from numpy.ma.core import shape
inception_resnet = InceptionResNetV2(weights="imagenet", include_top=False, 
                                     input_tensor=Input(shape=(224,224,3)))
inception_resnet.trainable=False
# --------------
headmodel = inception_resnet.output
headmodel = Flatten()(headmodel)
headmodel = Dense(500,activation="relu")(headmodel)
headmodel = Dense(250,activation="relu")(headmodel)
headmodel = Dense(4,activation='sigmoid')(headmodel)
# -------------- model
model = Model(inputs=inception_resnet.input,outputs=headmodel)

# compile model
model.compile(loss='mse', optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4))
model.summary()

# model training

from tensorflow.keras.callbacks import TensorBoard

tfb = TensorBoard('object_detection')

history = model.fit(x=x_train,y=y_train,batch_size=10,epochs=100,
                    validation_data=(x_test,y_test),callbacks=[tfb])

history = model.fit(x=x_train,y=y_train,batch_size=10,epochs=200,
                    validation_data=(x_test,y_test),callbacks=[tfb],initial_epoch=100)

model.save('./models/object_detection.h5')