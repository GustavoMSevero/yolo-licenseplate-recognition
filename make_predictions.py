import numpy as np
import cv2
import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow.keras.preprocessing.image import load_img, img_to_array

# load model
model = tf.keras.models.load_model('./models/object_detection.h5')
print('model loaded successfully')

path = './test_images/bentley1.jpeg'
image = load_img(path) # PIL Object
image = np.array(image,dtype=np.uint8) # 8 bit array (0,255)
image1 = load_img(path,target_size=(224,224))
image_arr_224 = img_to_array(image1)/255.0 # convert into array and get the normalized output

# size of the original image
h,w,d = image.shape
print('Height of the image = ',h)
print('Width of the image = ',w)

# Height of the image =  400
# Width of the image =  620

plt.figure(figsize=(10,8))
plt.imshow(image)
plt.show()

image_arr_224.shape

# (224, 224, 3)

test_arr = image_arr_224.reshape(1,224,224,3)
test_arr.shape

# (1, 224, 224, 3)

# make predictions
coords = model.predict(test_arr)
coords

# array([[0.3355198 , 0.656411  , 0.35033515, 0.45644563]], dtype=float32)

# denormalize the values
denorm = np.array([w,w,h,h])
coords = coords * denorm
coords

# array([[208.0222702 , 406.97481513, 140.13406038, 182.57825375]])

coords = coords.astype(np.int32)
coords

# array([[208, 406, 140, 182]], dtype=int32)

# draw bounding on the top image
xmin, xmax, ymin, ymax = coords[0]
pt1 = (xmin,ymin)
pt2 = (xmax, ymax)
print(pt1, pt2)
cv2.rectangle(image,pt1,pt2,(0,255,0),3)

plt.figure(figsize=(10,8))
plt.imshow(image)
plt.show()

# (208, 140) (406, 182)

