import pandas as pd
import xml.etree.ElementTree as xet

from glob import glob

path = glob('./images/*.xml')
path

labels_dict = dict(filepath=[],xmin=[],xmax=[],ymin=[],ymax=[])
for filename in path:
    # filename = path[0]
    info = xet.parse(filename)
    root = info.getroot()
    member_object = root.find('object')
    labels_info = member_object.find('bndbox')
    xmin = int(labels_info.find('xmin').text)
    xmax = int(labels_info.find('xmax').text)
    ymin = int(labels_info.find('ymin').text)
    ymax = int(labels_info.find('ymax').text)
    # print(xmin, xmax, ymin, ymax)
    labels_dict['filepath'].append(filename)
    labels_dict['xmin'].append(xmin)
    labels_dict['xmax'].append(xmax)
    labels_dict['ymin'].append(ymin)
    labels_dict['ymax'].append(ymax)

df = pd.DataFrame(labels_dict)
df


#           filepath	xmin	xmax	ymin	ymax
# 0	./images/N137.xml	395	595	453	503
# 1	./images/N208.xml	199	273	200	223
# 2	./images/N131.xml	197	350	105	145
# 3	./images/N102.xml	161	316	211	247
# 4	./images/N34.xml	558	903	479	600
# ...	...	...	...	...	...
# 223	./images/N113.xml	38	108	128	157
# 224	./images/N63.xml	295	424	168	224
# 225	./images/N160.xml	149	255	191	218
# 226	./images/N173.xml	81	331	155	249
# 227	./images/N149.xml	186	323	116	148
# 228 rows × 5 columns

df.to_csv('labels.csv',index=False)
